package com.talipov.controllers;

import com.talipov.common.exceptions.DaoException;
import com.talipov.services.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * Created by Марсель on 10.03.2017.
 */
@Controller
public class LoginController {
    static Logger logger = Logger.getLogger(LoginController.class);
    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/login")
    public String loginForm() {
        return "login";
    }

    @PostMapping(value = "/login")
    public String login(@RequestParam String login, @RequestParam String password, Model model) {
        logger.trace("post");

        try {
            if (userService.auth(login, password)) {
                logger.trace("authorized");
                return "redirect:/list";
            } else {
                logger.trace("not authorized");
                return "redirect:/login";
            }
        } catch (DaoException e) {
            logger.error(e);
            model.addAttribute("message", e.getMessage());
            return "error";
        }
    }
}
