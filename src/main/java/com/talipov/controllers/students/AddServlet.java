package com.talipov.controllers.students;

import com.talipov.common.exceptions.DaoException;
import org.apache.log4j.Logger;
import com.talipov.services.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Марсель on 23.02.2017.
 */
public class AddServlet extends HttpServlet {

    static Logger logger = Logger.getLogger(AddServlet.class);

    StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.trace("add post");
        String firstname = req.getParameter("firstname");
        String middlename = req.getParameter("middlename");
        String lastname = req.getParameter("lastname");
        String phone = req.getParameter("phone");
        String email = req.getParameter("email");

        try {
            if (studentService.create(firstname, middlename, lastname, phone, email)) {
                resp.sendRedirect("/students/list");
            } else {
                resp.sendRedirect("/error.jsp");
            }
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
        }
    }
}
