package com.talipov.controllers.lection;

import com.talipov.common.exceptions.DaoException;
import com.talipov.controllers.students.EditServlet;
import org.apache.log4j.Logger;
import com.talipov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Марсель on 24.02.2017.
 */
public class LectionDeleteServlet extends HttpServlet {
    static Logger logger = Logger.getLogger(EditServlet.class);

    LectionService lectionService;

    @Autowired
    public void setLectionService(LectionService lectionService) {
        this.lectionService = lectionService;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));

        try {
            if (lectionService.delete(id)) {
                resp.sendRedirect("/students/lection/list");
            } else {
                resp.sendRedirect("/error.jsp");
            }
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
        }
    }
}
