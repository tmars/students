package com.talipov.controllers.lection;

import com.talipov.common.exceptions.DaoException;
import com.talipov.models.pojo.Lection;
import org.apache.log4j.Logger;
import com.talipov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * Created by Марсель on 24.02.2017.
 */
public class LectionListServlet extends HttpServlet {

    static Logger logger = Logger.getLogger(LectionListServlet.class);

    LectionService lectionService;

    @Autowired
    public void setLectionService(LectionService lectionService) {
        this.lectionService = lectionService;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Lection> list;
        try {
            list = lectionService.getList();
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
            return;
        }
        req.setAttribute("lectionList", list);
        req.getRequestDispatcher("/lection/list.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    }
}
