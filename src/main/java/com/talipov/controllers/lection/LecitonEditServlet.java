package com.talipov.controllers.lection;

import com.talipov.common.exceptions.DaoException;
import com.talipov.controllers.students.EditServlet;
import com.talipov.models.pojo.Lection;
import org.apache.log4j.Logger;
import com.talipov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Марсель on 24.02.2017.
 */
public class LecitonEditServlet extends HttpServlet {

    static Logger logger = Logger.getLogger(EditServlet.class);

    LectionService lectionService;

    @Autowired
    public void setLectionService(LectionService lectionService) {
        this.lectionService = lectionService;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));
        Lection lection = null;
        try {
            lection = lectionService.getLectionById(id);
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
        }
        if (lection == null) {
            logger.error("lection not found");
            resp.sendRedirect("/error.jsp");
        }

        req.setAttribute("lection", lection);
        req.getRequestDispatcher("/lection/edit.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = Long.parseLong(req.getParameter("id"));

        String name = req.getParameter("name");
        String start = req.getParameter("started_at");
        if (name == null || start == null) {
            logger.error("Ошибка параметров");
            resp.sendRedirect("/error.jsp");
        }

        Timestamp startedAt = null;
        try {
            startedAt = new Timestamp(
                new SimpleDateFormat("yyyy-MM-dd'T'hh:mm").parse(start).getTime()
            );
        } catch (ParseException e) {
            logger.error("Ошибка параметров");
            resp.sendRedirect("/error.jsp");
        }

        try {
            if (lectionService.update(id, name, startedAt)) {
                resp.sendRedirect("/students/lection/list");
            } else {
                resp.sendRedirect("/error.jsp");
            }
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
        }
    }
}
