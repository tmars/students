package com.talipov.controllers.lection;

import com.talipov.common.exceptions.DaoException;
import com.talipov.controllers.students.AddServlet;
import org.apache.log4j.Logger;
import com.talipov.services.LectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Марсель on 24.02.2017.
 */
public class LectionAddServlet extends HttpServlet {

    static Logger logger = Logger.getLogger(AddServlet.class);

    LectionService lectionService;

    @Autowired
    public void setLectionService(LectionService lectionService) {
        this.lectionService = lectionService;
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/lection/add.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String start = req.getParameter("started_at");
        if (name == null || start == null) {
            logger.error("Ошибка параметров");
            resp.sendRedirect("/error.jsp");
        }

        Timestamp startedAt = null;
        try {
            startedAt = new Timestamp(
                    new SimpleDateFormat("yyyy-MM-dd'T'hh:mm").parse(start).getTime()
            );
        } catch (ParseException e) {
            logger.error("Ошибка параметров");
            resp.sendRedirect("/error.jsp");
        }

        try {
            if (lectionService.create(name, startedAt)) {
                resp.sendRedirect("/students/lection/list");
            } else {
                resp.sendRedirect("/error.jsp");
            }
        } catch (DaoException e) {
            logger.error(e);
            resp.sendRedirect("/error.jsp");
        }
    }

}
