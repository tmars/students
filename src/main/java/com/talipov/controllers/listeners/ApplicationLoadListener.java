package com.talipov.controllers.listeners;

import com.talipov.common.utils.LectionNotificator;
import org.apache.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 * Created by Марсель on 24.02.2017.
 */
public class ApplicationLoadListener implements ServletContextListener {

    private static final Logger logger = Logger.getLogger(ApplicationLoadListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        logger.trace("Site started");
        LectionNotificator.sendNotifications();
    }



    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        logger.trace("Site stopped");
    }


}
