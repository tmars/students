package com.talipov.controllers.listeners;

import com.talipov.common.mail.Mailer;
import com.talipov.models.pojo.User;
import org.apache.log4j.Logger;

import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Created by Марсель on 24.02.2017.
 */
public class SessionListener implements HttpSessionListener, HttpSessionAttributeListener {

    private static final Logger logger = Logger.getLogger(SessionListener.class);

    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        if (event.getName().equals("login")) {
            User user = null;
//            try {
//                user = UserService.getUserByLogin(event.getValue().toString());
//            } catch (DaoException e) {
//                logger.error(e);
//            }

            if (user != null && user.getEmail() != null && !user.getEmail().equals("")) {
                logger.trace("sendmail " + user.getEmail());
                Mailer.sendMail(user.getEmail(), "Был вход", "Только что вы вошли на сайт students");
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent event) {

    }

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        logger.trace("Session created");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }
}
