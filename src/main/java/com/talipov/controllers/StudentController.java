package com.talipov.controllers;

import com.talipov.common.exceptions.DaoException;
import com.talipov.services.StudentService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

/**
 * Created by Марсель on 10.03.2017.
 */
@Controller
public class StudentController {
    static Logger logger = Logger.getLogger(StudentController.class);
    StudentService studentService;

    @Autowired
    public void setStudentService(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping(value = "/list")
    public String list(Model model) {
        try {
            model.addAttribute("studentList", studentService.getList());
            return "list";
        } catch (DaoException e) {
            logger.error(e);
            return "error";
        }
    }
}
