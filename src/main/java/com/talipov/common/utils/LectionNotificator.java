package com.talipov.common.utils;

import com.talipov.common.exceptions.DaoException;
import com.talipov.common.mail.Mailer;
import com.talipov.models.pojo.Lection;
import com.talipov.models.pojo.Student;
import org.apache.log4j.Logger;
import com.talipov.services.LectionService;
import com.talipov.services.StudentService;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Марсель on 24.02.2017.
 */
public class LectionNotificator {

    private static ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
    private static final Logger logger = Logger.getLogger(LectionNotificator.class);


    public static void sendNotifications() {
        LectionService lectionService = new LectionService();
        StudentService studentService = new StudentService();
        try {
            List<Lection> lections = lectionService.getActiveList();
            List<Student> students = studentService.getList();

            for (Lection lection: lections) {
                for (Student student: students) {
                    Mailer.sendMail(student.getEmail(), "Скоро лекция", lection.getName());
                    break;
                }
            }
            logger.trace("lections count "+ lections.size());
        } catch (DaoException e) {
            logger.error(e);
        }
        pool.schedule(() -> sendNotifications(), 1, TimeUnit.HOURS);
    }
}
