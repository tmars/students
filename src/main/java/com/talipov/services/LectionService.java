package com.talipov.services;

import com.talipov.common.exceptions.LectionDaoException;
import com.talipov.models.dao.LectionDao;
import com.talipov.models.pojo.Lection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;

/**
 * Created by Марсель on 23.02.2017.
 */
@Service
public class LectionService {

    private LectionDao lectionDao;

    @Autowired
    public void setLectionDao(LectionDao lectionDao) {
        this.lectionDao = lectionDao;
    }

    public List<Lection> getList() throws LectionDaoException {
        return lectionDao.getList();
    }

    public List<Lection> getActiveList() throws LectionDaoException {
        return lectionDao.getActiveList();
    }

    public Lection getLectionById(long id) throws LectionDaoException {
        return lectionDao.getLectionById(id);
    }

    public boolean update(long id, String name, Timestamp startedAt)
            throws LectionDaoException {
        return lectionDao.update(id, name, startedAt);
    }

    public boolean delete(long id) throws LectionDaoException {
        return lectionDao.delete(id);
    }

    public boolean create(String name, Timestamp startedAt)
            throws LectionDaoException {
        return lectionDao.create(name, startedAt);
    }
}
