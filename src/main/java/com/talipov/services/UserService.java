package com.talipov.services;

import com.talipov.common.exceptions.UserDaoException;
import com.talipov.models.dao.UserDao;
import com.talipov.models.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Марсель on 23.02.2017.
 */
@Service
public class UserService {

    private UserDao userDao;

    @Autowired
    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public boolean auth(String login, String password) throws UserDaoException {
        if (userDao.getUserByLoginAndPassword(login, password) != null) {
            return true;
        }

        return false;
    }

    public boolean reg(String login, String password) throws UserDaoException {
        return userDao.regUser(login, password);
    }

    public User getUserByLogin(String login) throws UserDaoException {
        return userDao.getUserByLogin(login);
    }
}
