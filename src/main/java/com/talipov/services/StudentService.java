package com.talipov.services;

import com.talipov.common.exceptions.StudentDaoException;
import com.talipov.models.dao.StudentDao;
import com.talipov.models.pojo.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Марсель on 23.02.2017.
 */
@Service
public class StudentService {

    private StudentDao studentDao;

    @Autowired
    public void setStudentDao(StudentDao studentDao) {
        this.studentDao = studentDao;
    }

    public List<Student> getList() throws StudentDaoException {
        return studentDao.getList();
    }

    public Student getStudentById(long id) throws StudentDaoException {
        return studentDao.getStudentById(id);
    }

    public boolean update(long id, String firstname, String middlename, String lastname, String phone, String email)
            throws StudentDaoException {
        return studentDao.update(id, firstname, middlename, lastname, phone, email);
    }

    public boolean delete(long id) throws StudentDaoException {
        return studentDao.delete(id);
    }

    public boolean create(String firstname, String middlename, String lastname, String phone, String email)
            throws StudentDaoException {
        return studentDao.create(firstname, middlename, lastname, phone, email);
    }
}
