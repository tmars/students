package com.talipov.models.pojo;

import java.sql.Timestamp;

/**
 * Created by Марсель on 24.02.2017.
 */
public class Lection {

    private long id;
    private String name;
    private Timestamp startedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(Timestamp startedAt) {
        this.startedAt = startedAt;
    }

    public Lection(long id, String name, Timestamp startedAt) {

        this.id = id;
        this.name = name;
        this.startedAt = startedAt;
    }
}
