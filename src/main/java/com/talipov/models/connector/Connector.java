package com.talipov.models.connector;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Created by Марсель on 23.02.2017.
 */
public class Connector {
    private static final String DB_HOST = "jdbc:postgresql://localhost:5432/students";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "123";
    static {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConnection() throws SQLException {

        Properties p = new Properties();
        p.put("user", DB_USER);
        p.put("password", DB_PASSWORD);
        p.put("characterEncoding", "UTF-8");
        return DriverManager.getConnection(DB_HOST, p);
    }
}
