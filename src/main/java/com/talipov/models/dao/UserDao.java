package com.talipov.models.dao;

import com.talipov.common.exceptions.UserDaoException;
import com.talipov.models.connector.Connector;
import com.talipov.models.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by Марсель on 23.02.2017.
 */
@Repository
public class UserDao {

    private static final Logger logger = Logger.getLogger(UserDao.class);
    private static final String SQL_GET_USER = "SELECT * FROM  \"user\" WHERE login=? and password=?";
    private static final String SQL_GET = "SELECT * FROM  \"user\" WHERE login=?";
    private static final String SQL_CREATE_USER = "INSERT INTO \"user\"(login, password, role) VALUES(?,?,?)";

    public User getUserByLoginAndPassword(String login, String password) throws UserDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_GET_USER);
            statement.setString(1, login);
            statement.setString(2, password);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new User(
                        resultSet.getInt("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("role"),
                        resultSet.getString("email")
                );
            }

            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDaoException();
        }

        return null;
    }

    public User getUserByLogin(String login) throws UserDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_GET);
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new User(
                        resultSet.getInt("id"),
                        resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("role"),
                        resultSet.getString("email")
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDaoException();
        }

        return null;
    }

    public boolean regUser(String login, String password) throws UserDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE_USER);
            statement.setString(1, login);
            statement.setString(2, password);
            statement.setString(3, "user");
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new UserDaoException();
        }

        return false;
    }
}
