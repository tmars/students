package com.talipov.models.dao;

import com.talipov.common.exceptions.LectionDaoException;
import com.talipov.models.connector.Connector;
import com.talipov.models.pojo.Lection;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 23.02.2017.
 */
@Repository
public class LectionDao {

    private static final Logger logger = Logger.getLogger(LectionDao.class);

    private static final String SQL_GET_ACTIVE = "SELECT * FROM  \"lections\" " +
            "WHERE started_at > NOW() AND started_at < NOW() + INTERVAL '2 hours'";
    private static final String SQL_GET = "SELECT * FROM  \"lections\" WHERE id=?";
    private static final String SQL_ALL = "SELECT * FROM  \"lections\" ORDER BY id";
    private static final String SQL_UPDATE = "UPDATE lections " +
            "SET name=?,started_at=? WHERE id=?";
    private static final String SQL_DELETE = "DELETE FROM lections WHERE id=?";
    private static final String SQL_CREATE = "INSERT INTO lections(name, started_at) " +
            "VALUES(?,?)";

    public List<Lection> getList() throws LectionDaoException {
        List<Lection> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_ALL);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                list.add(
                        new Lection(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getTimestamp("started_at")
                        )
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return list;
    }

    public List<Lection> getActiveList() throws LectionDaoException {
        List<Lection> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_GET_ACTIVE);

            while (resultSet.next()) {
                list.add(
                        new Lection(
                                resultSet.getLong("id"),
                                resultSet.getString("name"),
                                resultSet.getTimestamp("started_at")
                        )
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return list;
    }


    public Lection getLectionById(long id) throws LectionDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_GET);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new Lection(
                        resultSet.getLong("id"),
                        resultSet.getString("name"),
                        resultSet.getTimestamp("started_at")
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return null;
    }

    public boolean update(long id, String name, Timestamp startedAt) throws LectionDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, name);
            statement.setTimestamp(2, startedAt);
            statement.setLong(3, id);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return false;
    }

    public boolean delete(long id) throws LectionDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, id);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return false;
    }

    public boolean create(String name, Timestamp startedAt) throws LectionDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE);
            statement.setString(1, name);
            statement.setTimestamp(2, startedAt);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new LectionDaoException();
        }

        return false;
    }
}