package com.talipov.models.dao;

import com.talipov.common.exceptions.StudentDaoException;
import com.talipov.models.connector.Connector;
import com.talipov.models.pojo.Student;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Марсель on 23.02.2017.
 */
@Repository
public class StudentDao {

    private static final Logger logger = Logger.getLogger(StudentDao.class);

    private static final String SQL_GET_STUDENT = "SELECT * FROM  \"students\" WHERE id=?";
    private static final String SQL_ALL_USERS = "SELECT * FROM  \"students\" ORDER BY id";
    private static final String SQL_UPDATE = "UPDATE students " +
            "SET firstname=?,middlename=?,lastname=?,phone=?,email=? WHERE id=?";
    private static final String SQL_DELETE = "DELETE FROM students WHERE id=?";
    private static final String SQL_CREATE = "INSERT INTO students(firstname, middlename, lastname, phone, email) " +
            "VALUES(?,?,?,?,?)";

    public List<Student> getList() throws StudentDaoException {
        List<Student> list = new ArrayList<>();
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_ALL_USERS);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                list.add(
                        new Student(
                                resultSet.getLong("id"),
                                resultSet.getString("firstname"),
                                resultSet.getString("middlename"),
                                resultSet.getString("lastname"),
                                resultSet.getString("email"),
                                resultSet.getString("phone")
                        )
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new StudentDaoException();
        }

        return list;
    }


    public Student getStudentById(long id) throws StudentDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_GET_STUDENT);
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                return new Student(
                        resultSet.getLong("id"),
                        resultSet.getString("firstname"),
                        resultSet.getString("middlename"),
                        resultSet.getString("lastname"),
                        resultSet.getString("email"),
                        resultSet.getString("phone")
                );
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new StudentDaoException();
        }

        return null;
    }

    public boolean update(long id, String firstname, String middlename, String lastname, String phone, String email)
            throws StudentDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, firstname);
            statement.setString(2, middlename);
            statement.setString(3, lastname);
            statement.setString(4, phone);
            statement.setString(5, email);
            statement.setLong(6, id);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new StudentDaoException();
        }

        return false;
    }

    public boolean delete(long id) throws StudentDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, id);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new StudentDaoException();
        }

        return false;
    }

    public boolean create(String firstname, String middlename, String lastname, String phone, String email)
            throws StudentDaoException {
        try (Connection connection = Connector.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(SQL_CREATE);
            statement.setString(1, firstname);
            statement.setString(2, middlename);
            statement.setString(3, lastname);
            statement.setString(4, phone);
            statement.setString(5, email);
            int count = statement.executeUpdate();

            if (count > 0) {
                return true;
            }
            statement.close();
        } catch (SQLException e) {
            logger.error(e);
            throw new StudentDaoException();
        }

        return false;
    }
}