<%--
  Created by IntelliJ IDEA.
  User: Марсель
  Date: 23.02.2017
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>Lection List</h1>
    <a href="/students/lection/add/">Add lection</a>
    <table border="1" cellpadding="5" cellspacing="0">
        <c:forEach items="${lectionList}" var="lection">
            <tr>
                <td><c:out value="${lection.id}"></c:out></td>
                <td><c:out value="${lection.name}"></c:out></td>
                <td><c:out value="${lection.startedAt}"></c:out></td>
                <td><a href="/students/lection/del/?id=<c:out value="${lection.id}"></c:out>">delete</a></td>
                <td><a href="/students/lection/edit/?id=<c:out value="${lection.id}"></c:out>">edit</a></td>
            </tr>
        </c:forEach>
    </table>

</body>
</html>
