<%@ page import="com.talipov.models.pojo.Lection" %>
<%@ page import="java.text.SimpleDateFormat" %><%--
  Created by IntelliJ IDEA.
  User: Марсель
  Date: 23.02.2017
  Time: 12:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>Lection Edit</h1>
    <% Lection lection = (Lection) request.getAttribute("lection"); %>
    <form action="/students/lection/edit/?id=<%= lection.getId() %>" method="post">
        <div>
            <label for="name">name</label>
            <input type="text" name="name" id="name" value="<%= lection.getName() %>">
        </div>
        <div>
            <label for="started_at">start at</label>
            <input type="datetime-local" name="started_at" id="started_at"
               value="<%= new SimpleDateFormat("yyyy-MM-dd'T'hh:mm").format(lection.getStartedAt()) %>">
        </div>
        <div>
            <input type="submit" value="Save">
        </div>
    </form>
</body>
</html>
