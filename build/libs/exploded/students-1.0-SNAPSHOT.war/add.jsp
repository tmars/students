<%@ page import="com.talipov.models.pojo.Student" %><%--
  Created by IntelliJ IDEA.
  User: Марсель
  Date: 23.02.2017
  Time: 12:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>Add</h1>
    <form action="/students/add/" method="post">
        <div>
            <label for="firstname">firstname</label>
            <input type="text" name="firstname" id="firstname">
        </div>
        <div>
            <label for="middlename">middlename</label>
            <input type="text" name="middlename" id="middlename">
        </div>
        <div>
            <label for="lastname">lastname</label>
            <input type="text" name="lastname" id="lastname">
        </div>
        <div>
            <label for="phone">phone</label>
            <input type="text" name="phone" id="phone">
        </div>
        <div>
            <label for="email">email</label>
            <input type="text" name="email" id="email">
        </div>
        <div>
            <input type="submit" value="Save">
        </div>
    </form>
</body>
</html>
