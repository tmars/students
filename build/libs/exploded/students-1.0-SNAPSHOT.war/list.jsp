<%--
  Created by IntelliJ IDEA.
  User: Марсель
  Date: 23.02.2017
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>List</h1>
    <a href="/students/add/">Add student</a>
    <table border="1" cellpadding="5" cellspacing="0">
        <c:forEach items="${studentList}" var="student">
            <tr>
                <td><c:out value="${student.id}"></c:out></td>
                <td><c:out value="${student.firstname}"></c:out></td>
                <td><c:out value="${student.middlename}"></c:out></td>
                <td><c:out value="${student.lastname}"></c:out></td>
                <td><c:out value="${student.phone}"></c:out></td>
                <td><c:out value="${student.email}"></c:out></td>
                <td><a href="/students/del/?id=<c:out value="${student.id}"></c:out>">delete</a></td>
                <td><a href="/students/edit/?id=<c:out value="${student.id}"></c:out>">edit</a></td>
            </tr>
        </c:forEach>
    </table>

</body>
</html>
